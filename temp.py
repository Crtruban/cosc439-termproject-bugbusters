#Final Project
#COSC 439 Sec: 002
#Date: December 4, 2020
#Group: Bug Busters
#Group Members: Angina SHrestha, Benjamin King, Camryn Truban
#python3 temp.py to run
#!/usr/bin/python
#Package that allows us to communicate with temp sensor
import Adafruit_DHT
#Utilized for error checking
import time
#Using Adafruit package specify the sensor that needs to be utilized
sens = Adafruit_DHT.DHT11
#Specify the specific GPIO Pin used for the sensor9(Pin 7 = GPIO 4)
pin = 4
#Checks that a proper connection is present. If I were to unplug GPIO 4 this would fail and not enter the while loop
while True:
    #Will go to GPIO 4 and recieve input from the signal pin on the sensor. 
    humi, temp = Adafruit_DHT.read(sens, pin)
    if humi is not None and temp is not None:
        print("Humidity={1:0.1f}% Temp={0:0.1f}C  ".format(humi,temp))
        quit()
        #If the incorrect information is sent from the sensor(Or more likely I specified the incorrect pin since I have 2 devices connected)
    else:
        print("Sensor failure. Check wiring.");
    time.sleep(3);
