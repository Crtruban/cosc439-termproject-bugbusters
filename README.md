***Setup***

For this project you are expected to using a Raspery Pi 2 or later editions with a Raspian OS installed.

DHT 1122, and HS504 sensors can be connected at the same time. Be sure to make notes of which GPIO pins the Signal(For DHT), and Echo and Trigger are 
connected to.

The LCD must be connected at a different time due to power limitations. As such do not attempt to connect all three devices or risk permanetly damaging your pi.

***Device Circuits***

DHT11 Circuit - https://www.circuitbasics.com/wp-content/uploads/2015/12/How-to-Setup-the-DHT11-on-the-Raspberry-Pi-Three-pin-DHT11-Wiring-Diagram.png

HS504 Circuit - https://pi.lbbcdn.com/wp-content/uploads/2018/03/Distance-Sensor-Fritz.png

LCD 16x2 Circuit - https://pi.lbbcdn.com/wp-content/uploads/2016/09/Raspberry-Pi-LCD-16x2-Circuit-Diagram-v1.png

***Pacakge Installation***

Install the Adafruit Package with the following commands:

_git clone https://github.com/adafruit/Adafruit_Python_DHT.git && cd Adafruit_Python_DHT_

_sudo python setup.py install_

Install the Adafruit LCD Package with the following commands:

git clone https://github.com/adafruit/Adafruit_Python_CharLCD.git

_sudo python setup.py install_


Download the repository, and 'cd' into it
If DHT11 or HC-SR04 are both connected you may perform the following steps if your Pi has the LCD connected skip to LCD Section

***HC-SR04***

Open the code in your prefered editor, and ensure that the correct GPIO Pins are set in lines 14, and 16 if you are unsure of the pins use command "pinout" for an overview
To run the code that uses the HC-SR04 type in the command "python temp.py"

***DHT11 / DHT22***

Open temp.py in your prefered editor, and ensure that the correct pin is set in line 15. If you are unsure of the pin you used, use command "pinout" for an overview of the GPIO.
To run the code that uses the DHT11 or DHT22 type in the command "distance.py"(Note if you did not install the ADAFruit package this code will not work)

***LCD***

Be sure that no other device besides the LCD is connected. If so, disconnect those devices.
If you are not following the diagram, open the LCD_Display_Driver.py in your preffered editor, and check that all the pins are where you have connected them. It is highly recommended that you follow the circuit provided else errors could occur.
TO run the LCD driver type in the command "python LCD_Display_Driver.py"
