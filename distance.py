#Final Project
#COSC 439 Sec: 002
#Date: December 4, 2020
#Group: Bug Busters
#Group Members: Angina SHrestha, Benjamin King, Camryn Truban
#python distance.py to run
#Package used to talk to pins 
import RPi.GPIO as GPIO
#Tme is used for error checking
import time
#Telling GPIO that we are refering to the pins by the "Broadcom  SOC Channel"
GPIO.setmode(GPIO.BCM)
#Trigger of the SR04 is on GPIO 23 or pin 16)
TRIG = 23
#Echo of the SR04 is omn GPIO 24 or pin 18)
ECHO = 24
#This was here to inform setup of the GPIO was succesful, if edited otherwise I would not see the measurment message
print "Distance Measurement in Progress..."
#The GPIO pins take both input and output, the trigger on the device takes in a relay input from the closest object to it.
GPIO.setup(TRIG,GPIO.OUT)
#The GPIO pin connected to the echo of the SR04 takes an output from the pi.  
GPIO.setup(ECHO,GPIO.IN)
#The false flag is setting the output state at 'High'
GPIO.output(TRIG, False)
print "Waiting for sensor to settle.."
time.sleep(2)
#Now we set the output state to 'High' with the 'TRUE' value.
GPIO.output(TRIG,True)
time.sleep(0.00001)
#Finish gathering what data we need by setting the output state back to 'Low'
GPIO.output(TRIG, False)
           #The Pi tells the echo trigger it's being used, the 0 is the low of the input state
while GPIO.input(ECHO)==0:
#To calculate the distance we need a start time of when we sent the input in
    pulse_start = time.time()
           #When the nput is set to high, we know the sensor hit an object. 
while GPIO.input(ECHO)==1:
    #This new time is utilized to calculate the distance
    pulse_end = time.time()
#The calculations are done by the pi to figure out how long a given pulse took the difference from the low to high gives us the data to calculate distance
pulse_duration = pulse_end - pulse_start

distance = pulse_duration * 17150

distance = round(distance, 2)

print "Distance: ",distance,"cm"
           #The cleanup will shutdown all channels to ensure no accidental damage is caused to the GPIO pins, or the pi itself
GPIO.cleanup()
